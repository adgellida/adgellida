<p align="center">
  
![Visitor Badge](https://visitor-badge.laobi.icu/badge?page_id=adgellida.adgellida)
  
**Hey 👋, I'm adgellida**

I'm specialized in electronics and automation, although I defend myself well in computer environments.

My topics of interest are Ubuntu, Xiaomi, Android, Arduino, Raspberry Pi, Open Source, Odoo, etc.

<a href="https://twitter.com/adgellida">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitter.svg" />
</a>
                                                                                                                       
<a href="https://www.linkedin.com/in/adgellida/">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/linkedin.svg" />
</a>

<a href="https://t.me/adgellida">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/telegram.svg" />
</a>

<a href="https://www.twitch.tv/adgellida">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/twitch.svg" />
</a>

<a href="https://discord.com/users/280292967745454081">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/discord.svg" />
</a>

<a href="https://www.instagram.com/adgellida/">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/instagram.svg" />
</a>

<a href="https://www.youtube.com/c/AntonioDavidGellidaLavara">
  <img align="left" alt="" width="22px" src="https://cdn.jsdelivr.net/npm/simple-icons@v3/icons/youtube.svg" />
</a>

</p>

<br>
</br>

<p align="left">

Main developments
=============================================

* <img src="https://raw.githubusercontent.com/adgellida/ubunsys/master/logo.png" width="50">[ubunsys](https://github.com/adgellida/ubunsys) - Gives the user a powerful control center to toggle on and off many system settings in Ubuntu x64, it is able to change system configurations, system updates, executable improvements, small system fixes, and more at the touch of a button

* <img src="https://raw.githubusercontent.com/adgellida/winsys/master/logo.png" width="50">[winsys](https://github.com/adgellida/winsys) - Very similiar tu ubunsys, but for Windows 10 x64

* <img src="https://raw.githubusercontent.com/adgellida/data-protection-list/main/images/privacy-first.png" width="50">[data-protection-list](https://github.com/adgellida/data-protection-list) - Manual of resistance to surveillance capitalism

Recommended repos
=============================================
* [TheNEWTTS](https://github.com/LuisSanchez-Dev/TheNewTTS) - The best Text to Speech (TTS) for Twitch, Mixer and Youtube streams for Streamlabs Chatbot

Telegram groups & Discord servers
=============================================
  <!-- Petshare -->
* <img src="https://github.com/adgellida/resources/blob/master/images/Petshare_room_Logo2.png" width="100"><a href="https://discord.gg/XFVCuW" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/discord.png" width="50"></a><a href="https://t.me/petshareroom" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/telegram.png" width="50"></a>

  <!-- Techshare -->
* <img src="https://github.com/adgellida/resources/blob/master/images/Techshare_room_Logo2.png" width="100"><a href="https://discord.gg/hN6MMK" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/discord.png" width="50"></a><a href="https://t.me/techshareroom" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/telegram.png" width="50"></a>

  <!-- Motorshare -->
* <img src="https://github.com/adgellida/resources/blob/master/images/Motorshare_room_Logo2.png" width="100"><a href="https://discord.gg/ATDRGu" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/discord.png" width="50"></a><a href="https://t.me/motorshareroom" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/telegram.png" width="50"></a>

  <!-- Personal -->
* <img src="https://github.com/adgellida/resources/blob/master/images/photo.jpg" width="100"><a href="https://discord.gg/6hRWpbG" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/discord.png" width="50"></a><a href="https://t.me/adgellida" target="_blank"><img src="https://github.com/adgellida/resources/blob/master/images/telegram.png" width="50"></a>

**Languages and Tools:**  

<code><img height="50" src="https://github.com/devicons/devicon/blob/master/icons/cplusplus/cplusplus-plain.svg"></code><code><img height="50" src="https://icon-icons.com/downloadimage.php?id=94938&root=1381/PNG/512/&file=qt_94938.png"></code>

<a href="https://github.com/adgellida/github-readme-stats">
  <img align="center" src="https://github-readme-stats.vercel.app/api?username=adgellida&show_icons=true&include_all_commits=true&theme=radical" alt="Anurag's github stats" />
</a>
<a href="https://github.com/adgellida/github-readme-stats">
  <!-- Change the `github-readme-stats.vercel.app` to `github-readme-stats.vercel.app`  -->
  <img align="center" src="https://github-readme-stats.vercel.app/api/top-langs/?username=adgellida&layout=compact&theme=radical" />
</a>

</p>
